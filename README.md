# Bitbucket Pipelines Example

Proof of concept for a build pipeline using Gradle, building and running tests for a Vert.x3 RESTful webservice.

----

**2016 Micha Kops / hasCode.com**